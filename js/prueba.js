
/*iniciate(   <String with data path>,      --> Path of file JSON with datas
*                 <width of svg>,               --> Width of panel SVG
*                 <height of svg>,              --> Height of panel SVG
*                 <array colours>,              --> Colours for bars
*                 <show data in bars>,          --> Show values in each bar
*                 <width of columns>            --> Width of columns. Only accept if columns_svg is false.
*                 <columns_svg>).               --> Width of columns calculated with width of SVG.
*/

function iniciar(){
      iniciate("datos_peso.json", 700, 200, "#555", ["steelBlue", "DarkGreen", "DarkOrange"], true, 50, false);
}
