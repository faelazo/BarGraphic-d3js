var data_import = [];

var path_data           = '';
var width_svg           = 600;
var height_svg          = 600;
var color_svg           = "rgba(255,255,255,0)";
var colours             = ["blue"];
var show_data           = true;
var width_col           = 30;
var columns_svg         = true;
var color_selected      = "SpringGreen";

//Init Template.
/*
* You can send empty param. So app get config to config.json.
* You can send this params:
*     iniciate(   <path_data>,                  --> String with path of file JSON with datas
*                 <width_svg>,                  --> Number with width of panel SVG
*                 <height_svg>,                 --> Number with height of panel SVG
*                 <color_svg>                   --> String with background-color of panel SVG
*                 <colours>,                    --> Array with colours for bars
*                 <show_data>,                  --> Boolean to show values in each bar
*                 <width_col>                   --> Number with width of columns. Only accept if columns_svg is false.
*                 <columns_svg>                 --> Boolean to width of columns calculated with width of SVG.
*                 <color_selected>)             --> Color to paint bar when it is selected.
*/
function iniciate(...params){

      //Load configuration
      if (params.length == 0){

            //get config data of config.json
            d3.json('config/config.json', function(err, data){
                  return data.json();
            }).then(function(data){
                  //Load configuration
                  load_config_json(data);
                  //Load data
                  load_data();
            });
      }else{
            //Load configuration
            load_config_array(params);
            //Load data
            load_data();
      };
};

//Load local variables from JSON.
function load_config_json(data){
      path_data         = data[0].path_data;
      width_svg         = data[0].width_svg;
      height_svg        = data[0].height_svg;
      color_svg         = data[0].color_svg;
      colours           = data[0].colours;
      show_data         = data[0].show_data;
      width_col         = data[0].width_col;
      columns_svg       = data[0].columns_svg;
      color_selected    = data[0].color_selected;
}

function load_config_array(data){
      path_data         = data[0];
      width_svg         = data[1];
      height_svg        = data[2];
      color_svg         = data[3];
      colours           = data[4];
      show_data         = data[5];
      width_col         = data[6];
      columns_svg       = data[7];
      color_selected    = data[8];
}

//load data.
function load_data(){
      d3.json(path_data, function(err, data){
            return data.json();
      }).then(function(data){
            data_import = data;
            //Draw Bar Graphic
            draw_graphic();
      });
}

//Draw Bar Graphic
function draw_graphic(){

      //Adapt width_col to width_svg.
      if (columns_svg){
            //width_col = (width_svg - margin-left - one pixel for bar - margin-right) / numbers of bars
            width_col = (width_svg - 30 - (data_import.length) - 30)/data_import.length;
      }

      //Scales
      var yRange = d3.scaleLinear()
                     .domain([0, d3.max(data_import, function(d){
                           return d.data;
                     })])
                     .range([0, (height_svg-20)]);

      var xRange = d3.scaleLinear()
                     .range([0, width_svg - 60 - 1])
                     .domain([0, data_import.length]);

      //Init SVG
      var svg = d3.select("body")
                  .append("svg")
                  .attr("width", width_svg)
                  .attr("height", height_svg)
                  .style("background-color", color_svg);

      //Load bars
      svg.selectAll("rect")
         .data(data_import)
         .enter()
         .append("rect")
         .attr("x", function(d,i){
               return i * (width_col + 1) + 31;
         })
         .attr("y", function(d){
               return height_svg - yRange(d.data) - 5;
         })
         .attr("width", width_col)
         .attr("height", function(d){
               return yRange(d.data);
         })
         .attr("fill", function(d,i){
               return colours[i%colours.length];
         })
         .on("mouseover", function(d, i){
               d3.select(this)
                 .attr("fill", color_selected);
         })
         .on("mouseout", function(d, i){
               d3.select(this)
                 .attr("fill", colours[i%colours.length]);
         });

      //Load data of bars
      if (show_data){
            svg.selectAll("text")
               .data(data_import)
               .enter()
               .append("text")
               .attr("y", function(d){
                     return height_svg - yRange(d.data) + 10;
               })
               .text(function(d){
                     return d.data;
               })
               .attr("x", function(d, i){
                     return i * (width_col + 1) + 30 + (width_col/2 - this.getComputedTextLength()/2);
               })
               .style("text-align", function(d){
                     return "center";
               });
      }
};
